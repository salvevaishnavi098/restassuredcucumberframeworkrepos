Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Sun, 17 Mar 2024 17:02:09 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"400","createdAt":"2024-03-17T17:02:09.494Z"}