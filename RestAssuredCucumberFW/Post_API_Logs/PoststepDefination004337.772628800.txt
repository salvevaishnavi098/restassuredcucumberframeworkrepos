Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Vaish",
    "job": "Leader"
}

Response header date is : 
Sun, 17 Mar 2024 19:13:38 GMT

Response body is : 
{"name":"Vaish","job":"Leader","id":"441","createdAt":"2024-03-17T19:13:38.079Z"}