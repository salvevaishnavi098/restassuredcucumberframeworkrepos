Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Vaish",
    "job": "Leader"
}

Response header date is : 
Sun, 17 Mar 2024 16:27:10 GMT

Response body is : 
{"name":"Vaish","job":"Leader","id":"334","createdAt":"2024-03-17T16:27:10.341Z"}