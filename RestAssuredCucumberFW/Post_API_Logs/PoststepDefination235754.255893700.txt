Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Vaish",
    "job": "Leader"
}

Response header date is : 
Sun, 17 Mar 2024 18:27:54 GMT

Response body is : 
{"name":"Vaish","job":"Leader","id":"978","createdAt":"2024-03-17T18:27:54.505Z"}