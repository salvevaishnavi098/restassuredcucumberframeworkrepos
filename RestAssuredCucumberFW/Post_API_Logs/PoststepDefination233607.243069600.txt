Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Sun, 17 Mar 2024 18:06:07 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"812","createdAt":"2024-03-17T18:06:07.482Z"}