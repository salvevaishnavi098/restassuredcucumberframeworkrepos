Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Vaish",
    "job": "Leader"
}

Response header date is : 
Sun, 17 Mar 2024 18:07:55 GMT

Response body is : 
{"name":"Vaish","job":"Leader","id":"827","createdAt":"2024-03-17T18:07:55.378Z"}