Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "zion resident"
}

Response header date is : 
Sun, 17 Mar 2024 16:27:18 GMT

Response body is : 
{"name":"morpheus","job":"zion resident","updatedAt":"2024-03-17T16:27:18.496Z"}