package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Comman_Methods.API_Trigger;
import Comman_Methods.Utility;
import Package_Respository.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class PutAPIDefination {

	Response response;
	int statuscode;
	String res_name;
	String res_job;
	String res_updatedAt;
	String requestBody;
	String Endpoint;
	String req_name;
	String req_job;

	@Given("{string} and {string} in Put request body")
	public void and_in_request_body(String req_name, String req_job) throws IOException {
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
		File dir_name = Utility.CreateLogDirectory("Put_API_Logs");
		requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job + "\"\r\n"
				+ "}";
		Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		response = API_Trigger.Put_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("PutstepDefination"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
	}

	@Given("Name and Job in request body in Put API")
	public void name_and_job_in_request_body() throws IOException {
		File dir_name = Utility.CreateLogDirectory("Put_API_Logs");
		requestBody = RequestBody.req_put_tc("Put_TC1");
		String Endpoint = RequestBody.Hostname() + RequestBody.PutResource();
		response = API_Trigger.Put_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("PutAPIDefination"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
		// throw new io.cucumber.java.PendingException();
	}

	@When("Sent the request with payload to the endpoint in Put API")
	public void sent_the_request_with_payload_to_the_endpoint_in_put_api() {
		statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		res_name =res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_updatedAt = response.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate status code in Put API")
	public void validate_status_code_in_put_api(){
		Assert.assertEquals(statuscode, 200);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Valdiate response body parameters in Put API")
	public void valdiate_response_body_parameters_in_put_api()  {
		JsonPath jsp_req = new JsonPath(requestBody);
		 req_name = jsp_req.getString("name");
		 req_job = jsp_req.getString("job");

		LocalDateTime curranttime = LocalDateTime.now();
		String expecteddate = curranttime.toString().substring(0, 11);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);
		// throw new io.cucumber.java.PendingException();
	}

}
