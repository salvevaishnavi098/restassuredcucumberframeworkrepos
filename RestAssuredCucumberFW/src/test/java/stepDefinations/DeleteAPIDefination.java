package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Comman_Methods.API_Trigger;
import Comman_Methods.Utility;
import Package_Respository.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

public class DeleteAPIDefination {
	
	int statuscode;
	Response response;
	String Endpoint;
	File dir_name;

	@Given("Enter Delete endpoint")
	public void enter_delete_endpoint() throws IOException {
		 dir_name = Utility.CreateLogDirectory("Delete_API_Log");
		 Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		response = API_Trigger.Delete_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Delete_API"), dir_name, Endpoint, null,
				response.getHeader("Date"), null);
	  // throw new io.cucumber.java.PendingException();
	}
	@When("Send the Delete API request")
	public void send_the_delete_api_request() {
	    statuscode = response.statusCode();
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate Delete Status code")
	public void validate_delete_status_code() {
			Assert.assertEquals(statuscode, 204);
	    //throw new io.cucumber.java.PendingException();
	}

}

