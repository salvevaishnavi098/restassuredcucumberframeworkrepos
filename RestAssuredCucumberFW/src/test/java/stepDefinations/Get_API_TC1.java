package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Comman_Methods.API_Trigger;
import Comman_Methods.Utility;
import Package_Respository.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Get_API_TC1 {
	
	int idArr[];
	String emailArr[];
	String fnamesArr[];
	String lnamesArr[];
	String Endpoint;
	File dir_name;
	Response response;
	String res_body;
	int statuscode;
	int count;
	JsonPath res_jsn;
	int i;
	
	int ids[] = { 7, 8, 9, 10, 11, 12 };
	String Emails[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
			"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
	String firstNames[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
	String lastNames[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };

	
	@Given("Enter Get endpoint")
	public void enter_get_endpoint() throws IOException {
		Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		 dir_name = Utility.CreateLogDirectory("Get_API_Log");
		response = API_Trigger.Get_trigger(RequestBody.HeaderName(),
				RequestBody.HeaderValue(), Endpoint);
				Utility.evidenceFileCreator(Utility.testLogName("get_list_user"), dir_name, Endpoint, null,
						response.getHeader("Date"), response.getBody().asPrettyString());
	   // throw new io.cucumber.java.PendingException();
	}
	@When("Send the Get API request")
	public void send_the_get_api_request() {
			res_body = response.getBody().asPrettyString();
			statuscode = response.getStatusCode();
			
			int idArr[] = new int[count];
			String emailArr[] = new String[count];
			String fnamesArr[] = new String[count];
			String lnamesArr[] = new String[count];

			for (int i = 0; i < count; i++) {
				{
					System.out.println("\n" + "---------fetched array data from arrays of expected data ---------");

					int res_id = res_jsn.getInt("data[" + i + "].id");
					System.out.println("\n" + res_id);
					idArr[i] = res_id;

					String res_email = res_jsn.get("data[" + i + "].email");
					System.out.println(res_email);
					emailArr[i] = res_email;

					String res_firstName = res_jsn.getString("data[" + i + "].first_name");
					System.out.println(res_firstName);
					fnamesArr[i] = res_firstName;

					String res_lastName = res_jsn.getString("data[" + i + "].last_name");
					System.out.println(res_lastName);
					lnamesArr[i] = res_lastName;
				}
				}
	}
	
	@Then("Validate Get status code")
	public void validate_get_status_code() {
			Assert.assertEquals(statuscode, 200);

	   // throw new io.cucumber.java.PendingException();
	}
	@Then("Valdiate response body parameters of Get API")
	public void valdiate_response_body_parameters_of_get_api() {
		res_jsn = new JsonPath(res_body);
		count = res_jsn.getInt("data.size()");
		
		int idArr[] = new int[count];
		String emailArr[] = new String[count];
		String fnamesArr[] = new String[count];
		String lnamesArr[] = new String[count];

		for (int i = 0; i < count; i++) {
			{

				int res_id = res_jsn.getInt("data[" + i + "].id");
				System.out.println("\n" + res_id);
				idArr[i] = res_id;

				String res_email = res_jsn.get("data[" + i + "].email");
				System.out.println(res_email);
				emailArr[i] = res_email;

				String res_firstName = res_jsn.getString("data[" + i + "].first_name");
				System.out.println(res_firstName);
				fnamesArr[i] = res_firstName;

				String res_lastName = res_jsn.getString("data[" + i + "].last_name");
				System.out.println(res_lastName);
				lnamesArr[i] = res_lastName;
			}
		}
		
		Assert.assertEquals(ids[i], idArr[i]);
		Assert.assertEquals(Emails[i], emailArr[i]);
		Assert.assertEquals(firstNames[i], fnamesArr[i]);
		Assert.assertEquals(lastNames[i], lnamesArr[i]);

  // throw new io.cucumber.java.PendingException();
	}
}

