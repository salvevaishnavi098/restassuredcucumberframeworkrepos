package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Comman_Methods.API_Trigger;
import Comman_Methods.Utility;
import Package_Respository.RequestBody;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class PoststepDefination {

	Response response;
	int statuscode;
	String res_name;
	String res_job;
	String res_id;
	String res_createdAt;
	String requestBody;
	String Endpoint;
	File dir_name;
	
	@Before
	public void Setup() {
		System.out.println("Function perform before test case");
	}
	
	@After
	public void Nextstep() {
		System.out.println("Function perform after test case");
	}

	@Given("{string} and {string} in request body")
	public void and_in_request_body(String req_name, String req_job) throws IOException {
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
		 dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job + "\"\r\n"
				+ "}";
		 Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("PoststepDefination"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
	}

	@Given("Name and Job in request body")
	public void name_and_job_in_request_body() throws IOException {
		File dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		requestBody = RequestBody.req_post_tc("Post_TC1");
		String Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("PoststepDefination"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
		// throw new io.cucumber.java.PendingException();
	}

	@When("Sent the request with payload to the endpoint")
	public void sent_the_request_with_payload_to_the_endpoint() {
		statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		res_name = res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_id = res_body.jsonPath().getString("id");
		res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate ststus code")
	public void validate_ststus_code() {
		Assert.assertEquals(statuscode, 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Valdiate response body parameters")
	public void valdiate_response_body_parameters() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);
		// throw new io.cucumber.java.PendingException();
	}

}
