package cucumberOptions;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/Features", glue = {
		"stepDefinations" })
//tags = "@DataDriven or @Post_API_Testcases or @Patch_API_Testcases or @Put_API_Testcases or @Delete_API_TestCases or @Get_API_Testcases")
public class TestRunner {

}
