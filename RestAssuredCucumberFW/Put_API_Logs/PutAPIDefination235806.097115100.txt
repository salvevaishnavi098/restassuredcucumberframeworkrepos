Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "zion resident"
}

Response header date is : 
Sun, 17 Mar 2024 18:28:06 GMT

Response body is : 
{"name":"morpheus","job":"zion resident","updatedAt":"2024-03-17T18:28:06.431Z"}