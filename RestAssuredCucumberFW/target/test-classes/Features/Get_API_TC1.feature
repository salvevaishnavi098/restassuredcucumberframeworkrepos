Feature: Trigger Get API

@Get_API_Testcases
Scenario: Trigger the Get API and Validate the parameters
		Given Enter Get endpoint
		When Send the Get API request
		Then Validate Get status code
		  And Valdiate response body parameters of Get API